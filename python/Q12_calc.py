from FixedPoint import FXfamily, FXnum
import argparse

parser = argparse.ArgumentParser(description='Calculate fixed point \
                                              representation of fractional \
                                              numbers')
parser.add_argument('-n', '--number', type = float, default = None,
                    help = 'Number to represent')
parser.add_argument('-f', '--frac_bits', type = int, default = 12,
                    help = 'Number of bits used to represent fractional part')
parser.add_argument('-i', '--int_bits', type = int, default = 4,
                    help = 'Number of bits used to represent integer part')

if __name__ == "__main__":
    args = parser.parse_args()
    fam = FXfamily(args.frac_bits, args.int_bits)

    if args.number is not None:
        print(fam(args.number).toBinaryString())
    else:
        values = [-0.98078, -0.83146, -0.55557, -0.19509, 0.19509, 0.55557,
                   0.83146, 0.98078]
        for val in values:
            print(fam(val).toBinaryString())
