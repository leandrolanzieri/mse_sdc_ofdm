## Sistemas Digitales para las Comunicaciones
### Maestría en Sistemas Embebidos | UBA
Repositorio del trabajo final para la materia Sistemas Digitales para las
Comunicaciones. Consta de la implementación de un bucle OFDM en FPGA.

![OFDM Loop](OFDM.png)