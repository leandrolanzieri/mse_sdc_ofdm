/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0xfbc00daa */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "/home/leandro/Documents/MSE/tp_final_sistemas_comunicacion/tp_final.srcs/sources_1/new/demapper_16_psk.vhd";
extern char *IEEE_P_1242562249;
extern char *IEEE_P_2592010699;

int ieee_p_1242562249_sub_1871261289446890672_1035706684(char *, char *, char *);
unsigned char ieee_p_2592010699_sub_2763492388968962707_503743352(char *, char *, unsigned int , unsigned int );


static void work_a_1555423618_1861270268_p_0(char *t0)
{
    char *t1;
    char *t2;
    int t3;
    char *t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    char *t9;

LAB0:    xsi_set_current_line(65, ng0);

LAB3:    t1 = (t0 + 1512U);
    t2 = *((char **)t1);
    t1 = (t0 + 6912U);
    t3 = ieee_p_1242562249_sub_1871261289446890672_1035706684(IEEE_P_1242562249, t2, t1);
    t4 = (t0 + 3928);
    t5 = (t4 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((int *)t8) = t3;
    xsi_driver_first_trans_fast(t4);

LAB2:    t9 = (t0 + 3832);
    *((int *)t9) = 1;

LAB1:    return;
LAB4:    goto LAB2;

}

static void work_a_1555423618_1861270268_p_1(char *t0)
{
    char *t1;
    unsigned char t2;
    char *t3;
    char *t4;
    unsigned char t5;
    unsigned char t6;
    char *t7;
    char *t8;
    char *t9;
    char *t10;
    int t11;
    unsigned char t12;
    int t13;
    unsigned char t14;
    char *t15;
    char *t16;
    char *t17;
    int t18;
    int t19;

LAB0:    xsi_set_current_line(69, ng0);
    t1 = (t0 + 992U);
    t2 = ieee_p_2592010699_sub_2763492388968962707_503743352(IEEE_P_2592010699, t1, 0U, 0U);
    if (t2 != 0)
        goto LAB2;

LAB4:
LAB3:    t1 = (t0 + 3848);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(70, ng0);
    t3 = (t0 + 1192U);
    t4 = *((char **)t3);
    t5 = *((unsigned char *)t4);
    t6 = (t5 == (unsigned char)3);
    if (t6 != 0)
        goto LAB5;

LAB7:    xsi_set_current_line(75, ng0);
    t1 = (t0 + 1352U);
    t3 = *((char **)t1);
    t2 = *((unsigned char *)t3);
    t5 = (t2 == (unsigned char)3);
    if (t5 != 0)
        goto LAB8;

LAB10:    xsi_set_current_line(147, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);

LAB9:
LAB6:    goto LAB3;

LAB5:    xsi_set_current_line(72, ng0);
    t3 = (t0 + 3992);
    t7 = (t3 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    *((unsigned char *)t10) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t3);
    xsi_set_current_line(73, ng0);
    t1 = xsi_get_transient_memory(4U);
    memset(t1, 0, 4U);
    t3 = t1;
    memset(t3, (unsigned char)2, 4U);
    t4 = (t0 + 4056);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 4U);
    xsi_driver_first_trans_fast_port(t4);
    goto LAB6;

LAB8:    xsi_set_current_line(78, ng0);
    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t11 = *((int *)t4);
    t12 = (t11 > 0);
    if (t12 == 1)
        goto LAB14;

LAB15:    t6 = (unsigned char)0;

LAB16:    if (t6 != 0)
        goto LAB11;

LAB13:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 1608);
    if (t5 == 1)
        goto LAB19;

LAB20:    t2 = (unsigned char)0;

LAB21:    if (t2 != 0)
        goto LAB17;

LAB18:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 3216);
    if (t5 == 1)
        goto LAB24;

LAB25:    t2 = (unsigned char)0;

LAB26:    if (t2 != 0)
        goto LAB22;

LAB23:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 4825);
    if (t5 == 1)
        goto LAB29;

LAB30:    t2 = (unsigned char)0;

LAB31:    if (t2 != 0)
        goto LAB27;

LAB28:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 6433);
    if (t5 == 1)
        goto LAB34;

LAB35:    t2 = (unsigned char)0;

LAB36:    if (t2 != 0)
        goto LAB32;

LAB33:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 8042);
    if (t5 == 1)
        goto LAB39;

LAB40:    t2 = (unsigned char)0;

LAB41:    if (t2 != 0)
        goto LAB37;

LAB38:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 9050);
    if (t5 == 1)
        goto LAB44;

LAB45:    t2 = (unsigned char)0;

LAB46:    if (t2 != 0)
        goto LAB42;

LAB43:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 11259);
    if (t5 == 1)
        goto LAB49;

LAB50:    t2 = (unsigned char)0;

LAB51:    if (t2 != 0)
        goto LAB47;

LAB48:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 12867);
    if (t5 == 1)
        goto LAB54;

LAB55:    t2 = (unsigned char)0;

LAB56:    if (t2 != 0)
        goto LAB52;

LAB53:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 14476);
    if (t5 == 1)
        goto LAB59;

LAB60:    t2 = (unsigned char)0;

LAB61:    if (t2 != 0)
        goto LAB57;

LAB58:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 16084);
    if (t5 == 1)
        goto LAB64;

LAB65:    t2 = (unsigned char)0;

LAB66:    if (t2 != 0)
        goto LAB62;

LAB63:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t5 = (t11 > 17693);
    if (t5 == 1)
        goto LAB69;

LAB70:    t2 = (unsigned char)0;

LAB71:    if (t2 != 0)
        goto LAB67;

LAB68:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t13 = (-(6433));
    t5 = (t11 > t13);
    if (t5 == 1)
        goto LAB74;

LAB75:    t2 = (unsigned char)0;

LAB76:    if (t2 != 0)
        goto LAB72;

LAB73:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t13 = (-(4825));
    t5 = (t11 > t13);
    if (t5 == 1)
        goto LAB79;

LAB80:    t2 = (unsigned char)0;

LAB81:    if (t2 != 0)
        goto LAB77;

LAB78:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t13 = (-(3216));
    t5 = (t11 > t13);
    if (t5 == 1)
        goto LAB84;

LAB85:    t2 = (unsigned char)0;

LAB86:    if (t2 != 0)
        goto LAB82;

LAB83:    t1 = (t0 + 1992U);
    t3 = *((char **)t1);
    t11 = *((int *)t3);
    t13 = (-(1608));
    t5 = (t11 > t13);
    if (t5 == 1)
        goto LAB89;

LAB90:    t2 = (unsigned char)0;

LAB91:    if (t2 != 0)
        goto LAB87;

LAB88:    xsi_set_current_line(142, ng0);
    t1 = (t0 + 7060);
    t4 = (t0 + 4056);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    memcpy(t10, t1, 4U);
    xsi_driver_first_trans_fast_port(t4);
    xsi_set_current_line(143, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)2;
    xsi_driver_first_trans_fast_port(t1);

LAB12:    goto LAB9;

LAB11:    xsi_set_current_line(79, ng0);
    t1 = (t0 + 6996);
    t9 = (t0 + 4056);
    t10 = (t9 + 56U);
    t15 = *((char **)t10);
    t16 = (t15 + 56U);
    t17 = *((char **)t16);
    memcpy(t17, t1, 4U);
    xsi_driver_first_trans_fast_port(t9);
    xsi_set_current_line(80, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB14:    t1 = (t0 + 1992U);
    t7 = *((char **)t1);
    t13 = *((int *)t7);
    t14 = (t13 <= 1608);
    t6 = t14;
    goto LAB16;

LAB17:    xsi_set_current_line(83, ng0);
    t1 = (t0 + 7000);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(84, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB19:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 3216);
    t2 = t6;
    goto LAB21;

LAB22:    xsi_set_current_line(87, ng0);
    t1 = (t0 + 7004);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(88, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB24:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 4825);
    t2 = t6;
    goto LAB26;

LAB27:    xsi_set_current_line(91, ng0);
    t1 = (t0 + 7008);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(92, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB29:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 6433);
    t2 = t6;
    goto LAB31;

LAB32:    xsi_set_current_line(95, ng0);
    t1 = (t0 + 7012);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(96, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB34:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 8042);
    t2 = t6;
    goto LAB36;

LAB37:    xsi_set_current_line(99, ng0);
    t1 = (t0 + 7016);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(100, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB39:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 9050);
    t2 = t6;
    goto LAB41;

LAB42:    xsi_set_current_line(103, ng0);
    t1 = (t0 + 7020);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(104, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB44:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 11259);
    t2 = t6;
    goto LAB46;

LAB47:    xsi_set_current_line(107, ng0);
    t1 = (t0 + 7024);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(108, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB49:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 12867);
    t2 = t6;
    goto LAB51;

LAB52:    xsi_set_current_line(111, ng0);
    t1 = (t0 + 7028);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(112, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB54:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 14476);
    t2 = t6;
    goto LAB56;

LAB57:    xsi_set_current_line(115, ng0);
    t1 = (t0 + 7032);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(116, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB59:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 16084);
    t2 = t6;
    goto LAB61;

LAB62:    xsi_set_current_line(119, ng0);
    t1 = (t0 + 7036);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(120, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB64:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 17693);
    t2 = t6;
    goto LAB66;

LAB67:    xsi_set_current_line(123, ng0);
    t1 = (t0 + 7040);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(124, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB69:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t13 = *((int *)t4);
    t6 = (t13 <= 19301);
    t2 = t6;
    goto LAB71;

LAB72:    xsi_set_current_line(127, ng0);
    t1 = (t0 + 7044);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(128, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB74:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t18 = *((int *)t4);
    t19 = (-(4825));
    t6 = (t18 <= t19);
    t2 = t6;
    goto LAB76;

LAB77:    xsi_set_current_line(131, ng0);
    t1 = (t0 + 7048);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(132, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB79:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t18 = *((int *)t4);
    t19 = (-(3216));
    t6 = (t18 <= t19);
    t2 = t6;
    goto LAB81;

LAB82:    xsi_set_current_line(135, ng0);
    t1 = (t0 + 7052);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(136, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB84:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t18 = *((int *)t4);
    t19 = (-(1608));
    t6 = (t18 <= t19);
    t2 = t6;
    goto LAB86;

LAB87:    xsi_set_current_line(139, ng0);
    t1 = (t0 + 7056);
    t8 = (t0 + 4056);
    t9 = (t8 + 56U);
    t10 = *((char **)t9);
    t15 = (t10 + 56U);
    t16 = *((char **)t15);
    memcpy(t16, t1, 4U);
    xsi_driver_first_trans_fast_port(t8);
    xsi_set_current_line(140, ng0);
    t1 = (t0 + 3992);
    t3 = (t1 + 56U);
    t4 = *((char **)t3);
    t7 = (t4 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)3;
    xsi_driver_first_trans_fast_port(t1);
    goto LAB12;

LAB89:    t1 = (t0 + 1992U);
    t4 = *((char **)t1);
    t18 = *((int *)t4);
    t19 = (-(0));
    t6 = (t18 <= t19);
    t2 = t6;
    goto LAB91;

}


extern void work_a_1555423618_1861270268_init()
{
	static char *pe[] = {(void *)work_a_1555423618_1861270268_p_0,(void *)work_a_1555423618_1861270268_p_1};
	xsi_register_didat("work_a_1555423618_1861270268", "isim/ofdm_top_tb.exe.sim/work/a_1555423618_1861270268.didat");
	xsi_register_executes(pe);
}
