--------------------------------------------------------------------------------
-- Company: 
-- Engineer:    Leandro Lanzieri Rodriguez
-- 
-- Create Date: 05/25/2019 06:35:19 PM
-- Design Name: 
-- Module Name: ofdm_top_tb - ofdm_top_tb_arch
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
--------------------------------------------------------------------------------

library ieee ;
  use ieee.std_logic_1164.all ;
  use ieee.numeric_std.all ;

entity ofdm_top_tb is
end ofdm_top_tb ; 

architecture ofdm_top_tb_arch of ofdm_top_tb is

  ------------------------------------------------------------------------------
  -- Components
  ------------------------------------------------------------------------------
  -- OFDM top module declaration
  component ofdm_top
    port (
      clk_sys      : in std_logic;
      clk_2x_sys   : in std_logic;
      fx2_rst      : in std_logic;
      fx2_clk      : in std_logic;
      h2fReady_in  : out std_logic;
      h2fData_out  : in std_logic_vector(7 downto 0);
      h2fValid_out : in std_logic;
      f2hData_in   : out std_logic_vector(7 downto 0);
      f2hValid_in  : out std_logic;
      f2hReady_out : in std_logic
    );
  end component;

  ------------------------------------------------------------------------------
  -- Signals
  ------------------------------------------------------------------------------
  -- Input to module under test
  signal clk_sys      : std_logic := '0';
  signal clk_2x_sys   : std_logic := '0';
  signal fx2_rst      : std_logic := '0';
  signal fx2_clk      : std_logic := '0';
  signal h2fData_out  : std_logic_vector(7 downto 0) := (others => '0');
  signal h2fValid_out : std_logic := '0';
  signal f2hReady_out : std_logic := '0';

  -- Ouput from module under test
  signal h2fReady_in : std_logic;
  signal f2hData_in  : std_logic_vector(7 downto 0);
  signal f2hValid_in : std_logic;

  ------------------------------------------------------------------------------
  -- Constants
  ------------------------------------------------------------------------------
  constant fx2_clk_period    : time := 22 ns;
  constant clk_sys_period    : time := 20 ns;
  constant clk_2x_sys_period : time := clk_sys_period / 2;

begin

  dut: ofdm_top
    port map(
      fx2_clk      => fx2_clk,
      fx2_rst      => fx2_rst,
      clk_sys      => clk_sys,
      clk_2x_sys   => clk_2x_sys,
      h2fData_out  => h2fData_out,
      h2fValid_out => h2fValid_out,
      h2fReady_in  => h2fReady_in,
      f2hData_in   => f2hData_in,
      f2hValid_in  => f2hValid_in,
      f2hReady_out => f2hReady_out
    );

    fx2_clk_process: process
    begin
      fx2_clk <= '0';
      wait for fx2_clk_period / 2;
      fx2_clk <= '1';
      wait for fx2_clk_period / 2;
    end process;

    clk_sys_process: process
    begin
      clk_sys <= '0';
      wait for clk_sys_period / 2;
      clk_sys <= '1';
      wait for clk_sys_period / 2;
    end process;

    clk_2x_sys_process: process
    begin
      clk_2x_sys <= '0';
      wait for clk_2x_sys_period / 2;
      clk_2x_sys <= '1';
      wait for clk_2x_sys_period / 2;
    end process;

    test_process: process
    begin
      -- reset for 5 clocks
      fx2_rst <= '1';
      wait for 5 * fx2_clk_period;
      fx2_rst <= '0';
      wait for fx2_clk_period * 3;

      wait for fx2_clk_period * 100;
      wait until fx2_clk = '1';

      for i in 0 to 16 loop
        h2fData_out <= std_logic_vector(to_unsigned(i, h2fData_out'length));
        h2fValid_out <= '1';
        wait for fx2_clk_period;
      end loop;

      h2fValid_out <= '0';
      wait for fx2_clk_period;
      f2hReady_out <= '1';

      wait; -- end test
    end process;

end architecture;