# Tcl script generated by PlanAhead

set reloadAllCoreGenRepositories false

set tclUtilsPath "/opt/Xilinx/14.7/ISE_DS/PlanAhead/scripts/pa_cg_utils.tcl"

set repoPaths ""

set cgIndexMapPath "/home/leandro/Documents/MSE/tp_final_sistemas_comunicacion/tp_final.srcs/sources_1/ip/cg_lin_index_map.xml"

set cgProjectPath "/home/leandro/Documents/MSE/tp_final_sistemas_comunicacion/tp_final.srcs/sources_1/ip/fifo_generator_v9_3_0/coregen.cgc"

set ipFile "/home/leandro/Documents/MSE/tp_final_sistemas_comunicacion/tp_final.srcs/sources_1/ip/fifo_generator_v9_3_0/fifo_in.xco"

set ipName "fifo_in"

set hdlType "VHDL"

set cgPartSpec "xc3s500e-4fg320"

set chains "GENERATE_CURRENT_CHAIN"

set params ""

set bomFilePath "/home/leandro/Documents/MSE/tp_final_sistemas_comunicacion/tp_final.srcs/sources_1/ip/fifo_generator_v9_3_0/pa_cg_bom.xml"

# generate the IP
set result [source "/opt/Xilinx/14.7/ISE_DS/PlanAhead/scripts/pa_cg_gen_out_prods.tcl"]

exit $result

