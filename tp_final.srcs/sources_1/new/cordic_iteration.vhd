----------------------------------------------------------------------------------
-- Company: 
-- Engineer:    Leandro Lanzieri Rodriguez
-- 
-- Create Date: 06/06/2019 10:25:30 PM
-- Design Name: 
-- Module Name: cordic_iteration - cordic_iteration_arch
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity cordic_iteration is
    generic (
        N     : natural := 8; -- word bits
        SHIFT : natural := 1
    );
    port ( 
        clk        : in std_logic;
        rst        : in std_logic;
        en         : in std_logic;
        x_in       : in std_logic_vector(N-1 downto 0);
        y_in       : in std_logic_vector(N-1 downto 0);
        z_in       : in std_logic_vector(N-1 downto 0);
        c_in       : in std_logic_vector(N-1 downto 0);
        data_valid : out std_logic;
        x_out      : out std_logic_vector(N-1 downto 0);
        y_out      : out std_logic_vector(N-1 downto 0);
        z_out      : out std_logic_vector(N-1 downto 0)
    );
end cordic_iteration;

architecture cordic_iteration_arch of cordic_iteration is
    signal x_in_shift : signed(N-1 downto 0);
    signal y_in_shift : signed(N-1 downto 0);
    signal x_in_sig   : signed(N-1 downto 0);
    signal y_in_sig   : signed(N-1 downto 0);
    signal z_in_sig   : signed(N-1 downto 0);
    signal c_in_sig   : signed(N-1 downto 0);
begin

    x_in_sig <= signed(x_in);
    y_in_sig <= signed(y_in);
    z_in_sig <= signed(z_in);
    c_in_sig <= signed(c_in);

    -- get the shifted signals for operations
    x_in_shift <= shift_right(x_in_sig, SHIFT);
    y_in_shift <= shift_right(y_in_sig, SHIFT);

    main_process: process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                -- reset output
                x_out <= (others => '0');
                y_out <= (others => '0');
                z_out <= (others => '0');
                data_valid <= '0';
            else
                data_valid <= en;
                if (en = '1') then
                    -- get input sign
                    if (y_in(N-1) = '1') then
                        -- d should be 1
                        x_out <= std_logic_vector(x_in_sig - y_in_shift);
                        y_out <= std_logic_vector(y_in_sig + x_in_shift);
                        z_out <= std_logic_vector(z_in_sig - c_in_sig);
                    else
                        -- d should be -1
                        x_out <= std_logic_vector(x_in_sig + y_in_shift);
                        y_out <= std_logic_vector(y_in_sig - x_in_shift);
                        z_out <= std_logic_vector(z_in_sig + c_in_sig);
                    end if;
                end if;
            end if;
        end if;
    end process;

end cordic_iteration_arch;
