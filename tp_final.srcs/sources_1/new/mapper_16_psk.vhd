library ieee;
    use ieee.std_logic_1164.all;
    use ieee.numeric_std.all;

entity mapper_16_psk is
  port (
    clk        : in std_logic;
    rst        : in std_logic;
    en         : in std_logic;
    data_in    : in std_logic_vector(3 downto 0);
    data_valid : out std_logic;
    i_out      : out std_logic_vector(15 downto 0); -- real part
    q_out      : out std_logic_vector(15 downto 0)  -- imaginary part
  );
end mapper_16_psk; 

architecture mapper_16_psk_arch of mapper_16_psk is
  constant N: natural := 16;
  -- define the type of the components of the constelation table
  type axis is array(0 to N-1) of integer range -2 ** (N-1) to 2 ** (N-1) - 1;

--------------------------------------------------------------------------------
-- Calculations: Input -> Degrees -> Real and Imaginary parts
--------------------------------------------------------------------------------

------------------------------------------------------
--  Input  --  Ang [deg]  --   I Comp  --   Q Comp  --
------------------------------------------------------
------------------------------------------------------
-- 0b0000  --    11.25    --   0.98078 --  0.19509  --
------------------------------------------------------
-- 0b0001  --    33.75    --   0.83146 --  0.55557  --
------------------------------------------------------
-- 0b0010  --    56.25    --   0.55557 --  0.83146  --
------------------------------------------------------
-- 0b0011  --    78.75    --   0.19509 --  0.98078  --
------------------------------------------------------
-- 0b0100  --   101.25    --  -0.19509 --  0.98078  --
------------------------------------------------------
-- 0b0101  --   123.75    --  -0.55557 --  0.83146  --
------------------------------------------------------
-- 0b0110  --   146.25    --  -0.83146 --  0.55557  --
------------------------------------------------------
-- 0b0111  --   168.75    --  -0.98078 --  0.19509  --
------------------------------------------------------
-- 0b1000  --   191.25    --  -0.98078 -- -0.19509  --
------------------------------------------------------
-- 0b1001  --   213.75    --  -0.83146 -- -0.55557  --
------------------------------------------------------
-- 0b1010  --   236.25    --  -0.55557 -- -0.83146  --
------------------------------------------------------
-- 0b1011  --   258.75    --  -0.19509 -- -0.98078  --
------------------------------------------------------
-- 0b1100  --   281.25    --   0.19509 -- -0.98078  --
------------------------------------------------------
-- 0b1101  --   303.75    --   0.55557 -- -0.83146  --
------------------------------------------------------
-- 0b1110  --   326.25    --   0.83146 -- -0.55557  --
------------------------------------------------------
-- 0b1111  --   348.75    --   0.98078 -- -0.19509  --
------------------------------------------------------


--------------------------------------------------------------------------------
-- Calculations: Q12 fixed point from projections
--------------------------------------------------------------------------------

-----------------------------------------------------------
--   Value   --  Int Part  --   Frac Part    --  Output  --
-----------------------------------------------------------
-----------------------------------------------------------
-- -0.98078  --   0b1111   -- 0b000001001111 --  -4017   --
-----------------------------------------------------------
-- -0.83146  --   0b1111   -- 0b001010110011 --  -3405   --
-----------------------------------------------------------
-- -0.55557  --   0b1111   -- 0b011100011101 --  -2275   --
-----------------------------------------------------------
-- -0.19509  --   0b1111   -- 0b110011100001 --   -799   --
-----------------------------------------------------------
--  0.19509  --   0b0000   -- 0b001100011111 --    799   --
-----------------------------------------------------------
--  0.55557  --   0b0000   -- 0b100011100011 --   2275   --
-----------------------------------------------------------
--  0.83146  --   0b0000   -- 0b110101001101 --   3405   --
-----------------------------------------------------------
--  0.98078  --   0b0000   -- 0b111110110001 --   4017   --
-----------------------------------------------------------

  signal i_table: axis := (4017, 3405, 2275, 799, -799, -2275, -3405, -4017,
                           -4017, -3405, -2275, -799, 799, 2275, 3405, 4017);

  signal q_table: axis := (799, 2275, 3405, 4017, 4017, 3405, 2275, 799, -799,
                           -2275, -3405, -4017, -4017, -3405, -2275, -799);
begin

  main_process: process(clk)
  begin
    if rising_edge(clk) then
      if (rst = '1') then
        -- reset data valid and outputs
        data_valid <= '0';
        i_out <= (others => '0');
        q_out <= (others => '0');
      else
        if (en = '1') then
          -- now data is valid, process new values
          i_out <= std_logic_vector(to_signed(i_table(to_integer(unsigned(data_in))), (N)));
          q_out <= std_logic_vector(to_signed(q_table(to_integer(unsigned(data_in))), (N)));
          data_valid <= '1';
        else
          -- if module is not enabled, data is not valid
          data_valid <= '0';
        end if;
      end if;
    end if;
  end process;

end architecture;