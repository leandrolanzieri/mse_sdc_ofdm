--------------------------------------------------------------------------------
-- Company:
-- Engineer:    Leandro Lanzieri Rodriguez
--
-- Create Date: 05/25/2019 04:05:23 PM
-- Design Name:
-- Module Name: ofdm_top - ofdm_top_arch
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
--------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity delay_i_q is
    generic (
        N : natural := 16  -- word bits
    );
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        en         : in std_logic;
        data_i_in  : in std_logic_vector(N-1 downto 0);
        data_q_in  : in std_logic_vector(N-1 downto 0);
        data_valid : out std_logic;
        data_i_out : out std_logic_vector(N-1 downto 0);
        data_q_out : out std_logic_vector(N-1 downto 0));
end delay_i_q;

architecture delay_i_q_arch of delay_i_q is
    signal data_q, data_i : std_logic_vector(N-1 downto 0);
begin

    main_process: process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                data_valid <= '0';
                data_i_out <= (others => '0');
                data_q_out <= (others => '0');
                data_q <= (others => '0');
                data_i <= (others => '0');
            else
                data_valid <= en;

                data_q <= data_q_in;
                data_q_out <= data_q;

                data_i <= data_i_in;
                data_i_out <= data_i;
            end if;
        end if;
    end process;
end delay_i_q_arch;
