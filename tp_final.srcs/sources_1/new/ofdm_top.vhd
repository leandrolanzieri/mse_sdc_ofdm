--------------------------------------------------------------------------------
-- Company:
-- Engineer:    Leandro Lanzieri Rodriguez
--
-- Create Date: 05/25/2019 04:05:23 PM
-- Design Name:
-- Module Name: ofdm_top - ofdm_top_arch
-- Project Name:
-- Target Devices:
-- Tool Versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
--------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity ofdm_top is
  port (
    clk_sys      : in std_logic;
    clk_2x_sys   : in std_logic;
    fx2_rst      : in std_logic;
    fx2_clk      : in std_logic;

    ----------------------------------------------------------------------------
    -- FPGAlink pipes
    ----------------------------------------------------------------------------
    -- Host to FPGA
    h2fReady_in  : out std_logic;
    h2fData_out  : in std_logic_vector(7 downto 0);
    h2fValid_out : in std_logic;

    -- FPGA to host
    f2hData_in   : out std_logic_vector(7 downto 0);
    f2hValid_in  : out std_logic;
    f2hReady_out : in std_logic
  );
end ofdm_top;

architecture ofdm_top_arch of ofdm_top is

  ------------------------------------------------------------------------------
  -- Components
  ------------------------------------------------------------------------------
  -- FIFO 8 bits to 1 bit
  component fifo_8_to_1
    port (
      rst    : in std_logic;
      wr_clk : in std_logic;
      rd_clk : in std_logic;
      din    : in std_logic_vector(7 downto 0);
      wr_en  : in std_logic;
      rd_en  : in std_logic;
      dout   : out std_logic_vector(0 downto 0);
      full   : out std_logic;
      empty  : out std_logic;
      valid  : out std_logic
    );
  end component;

  -- FIFO 1 bit to 8 bits
  component fifo_1_to_8
    port (
      rst    : in std_logic;
      wr_clk : in std_logic;
      rd_clk : in std_logic;
      din    : in std_logic_vector(0 downto 0);
      wr_en  : in std_logic;
      rd_en  : in std_logic;
      dout   : out std_logic_vector(7 downto 0);
      full   : out std_logic;
      empty  : out std_logic;
      valid  : out std_logic
    );
  end component;

  -- Convolutional encoder
  component encoder
  port(
    data_in    : in  std_logic;
    data_out_v : out std_logic_vector(1 downto 0);
    rdy        : out std_logic;
    nd         : in std_logic;
    ce         : in  std_logic;
    sclr       : in  std_logic;
    clk        : in  std_logic
  );
  end component;

  -- Viterbi decoder
  component viterbi
    port(
      data_in0 : in  std_logic_vector(0 downto 0);
      data_in1 : in  std_logic_vector(0 downto 0);
      data_out : out std_logic;
      rdy      : out std_logic;
      ce       : in  std_logic;
      sclr     : in  std_logic;
      clk      : in  std_logic
    );
  end component;
  
  -- FIFO 2 bits to 4 bits
    component fifo_2_to_4
    port (
      rst    : in std_logic;
      wr_clk : in std_logic;
      rd_clk : in std_logic;
      din    : in std_logic_vector(1 downto 0);
      wr_en  : in std_logic;
      rd_en  : in std_logic;
      dout   : out std_logic_vector(3 downto 0);
      full   : out std_logic;
      empty  : out std_logic;
      valid  : out std_logic
    );
  end component;

    -- FIFO 4 bits to 2 bits
    component fifo_4_to_2
    port (
      rst    : in std_logic;
      wr_clk : in std_logic;
      rd_clk : in std_logic;
      din    : in std_logic_vector(3 downto 0);
      wr_en  : in std_logic;
      rd_en  : in std_logic;
      dout   : out std_logic_vector(1 downto 0);
      full   : out std_logic;
      empty  : out std_logic;
      valid  : out std_logic
    );
  end component;

  -- 16 PSK mapper
  component mapper_16_psk is
    port (
      clk        : in std_logic;
      rst        : in std_logic;
      en         : in std_logic;
      data_in    : in std_logic_vector(3 downto 0);
      data_valid : out std_logic;
      i_out      : out std_logic_vector(15 downto 0); -- real part
      q_out      : out std_logic_vector(15 downto 0)  -- imaginary part
    );
  end component; 

  -- Cordic
  component cordic is
    generic (
        N    : natural := 16; -- word bits
        ITER : natural := 16  -- amount of iterations
    );
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        en         : in std_logic;
        x_in       : in std_logic_vector(N-1 downto 0);
        y_in       : in std_logic_vector(N-1 downto 0);
        data_valid : out std_logic;
        z_out      : out std_logic_vector(N-1 downto 0)
    );
  end component;

  -- 16 PSK demapper
  component demapper_16_psk is
    port (
      clk        : in std_logic;
      rst        : in std_logic;
      en         : in std_logic;
      data_in    : in std_logic_vector(15 downto 0);
      data_valid : out std_logic;
      data_out   : out std_logic_vector(3 downto 0)
    );
  end component; 

  -- Delay I Q
  component delay_i_q is
    generic (
        N : natural := 16  -- word bits
    );
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        en         : in std_logic;
        data_i_in  : in std_logic_vector(N-1 downto 0);
        data_q_in  : in std_logic_vector(N-1 downto 0);
        data_valid : out std_logic;
        data_i_out : out std_logic_vector(N-1 downto 0);
        data_q_out : out std_logic_vector(N-1 downto 0));
  end component;

  -- IFFT
  component ifft
    port (
      clk        : in std_logic;
      ce         : in std_logic;
      sclr       : in std_logic;
      start      : in std_logic;
      unload     : in std_logic;
      xn_re      : in std_logic_vector(15 downto 0);
      xn_im      : in std_logic_vector(15 downto 0);
      fwd_inv    : in std_logic;
      fwd_inv_we : in std_logic;
      rfd        : out std_logic;
      xn_index   : out std_logic_vector(5 downto 0);
      busy       : out std_logic;
      edone      : out std_logic;
      done       : out std_logic;
      dv         : out std_logic;
      xk_index   : out std_logic_vector(5 downto 0);
      xk_re      : out std_logic_vector(22 downto 0);
      xk_im      : out std_logic_vector(22 downto 0)
    );
  end component;

  --FFT
  component fft
    port (
      clk        : in std_logic;
      ce         : in std_logic;
      sclr       : in std_logic;
      start      : in std_logic;
      unload     : in std_logic;
      xn_re      : in std_logic_vector(22 downto 0);
      xn_im      : in std_logic_vector(22 downto 0);
      fwd_inv    : in std_logic;
      fwd_inv_we : in std_logic;
      rfd        : out std_logic;
      xn_index   : out std_logic_vector(5 downto 0);
      busy       : out std_logic;
      edone      : out std_logic;
      done       : out std_logic;
      dv         : out std_logic;
      xk_index   : out std_logic_vector(5 downto 0);
      xk_re      : out std_logic_vector(29 downto 0);
      xk_im      : out std_logic_vector(29 downto 0)
    );
  end component;

  ------------------------------------------------------------------------------
  -- Signals
  ------------------------------------------------------------------------------
  -- Common
  signal clk_sig, clk_2x_sig, rst_sig : std_logic;
  constant N_tb : integer := 16;
  constant ITER : natural := 7; -- cordic iterations

  -- Input FIFO (fifo_8_to_1)
  signal fifo_in_rd_en      : std_logic;
  signal fifo_in_empty      : std_logic;
  signal fifo_in_data_valid : std_logic;
  signal fifo_in_data_out   : std_logic_vector(0 downto 0);

  -- Convolutional encoder
  signal encoder_data_in     : std_logic;
  signal encoder_data_valid  : std_logic;
  signal encoder_new_data_in : std_logic;
  signal encoder_data_out    : std_logic_vector(1 downto 0);

  -- FIFO 2 bits to 4 bits
  signal fifo_2to4_rd_en      : std_logic;
  signal fifo_2to4_wr_en      : std_logic;
  signal fifo_2to4_empty      : std_logic;
  signal fifo_2to4_data_valid : std_logic;
  signal fifo_2to4_data_in    : std_logic_vector(1 downto 0);
  signal fifo_2to4_data_out   : std_logic_vector(3 downto 0);

  -- FIFO 4 bits to 2 bits
  signal fifo_4to2_rd_en      : std_logic;
  signal fifo_4to2_wr_en      : std_logic;
  signal fifo_4to2_empty      : std_logic;
  signal fifo_4to2_data_valid : std_logic;
  signal fifo_4to2_data_in    : std_logic_vector(3 downto 0);
  signal fifo_4to2_data_out   : std_logic_vector(1 downto 0);

  -- 16 PSK Mapper
  signal mapper_data_in             : std_logic_vector(3 downto 0);
  signal mapper_q_out, mapper_i_out : std_logic_vector(15 downto 0);
  signal mapper_en                  : std_logic;
  signal mapper_data_valid          : std_logic;

  -- Delay I Q for IFFT
  constant N_IFFT                           : integer := 16;    -- word bits for IFFT input
  signal delay_ifft_data_valid              : std_logic;
  signal delay_ifft_en                      : std_logic;
  signal delay_ifft_i_in, delay_ifft_q_in   : std_logic_vector(N_IFFT - 1 downto 0);
  signal delay_ifft_i_out, delay_ifft_q_out : std_logic_vector(N_IFFT - 1 downto 0);

  -- IFFT
  constant N_FFT                : integer := 23;    -- word bits for FFT input
  signal ifft_i_in, ifft_q_in   : std_logic_vector(N_IFFT - 1 downto 0);
  signal ifft_i_out, ifft_q_out : std_logic_vector(N_FFT - 1 downto 0);
  signal ifft_start             : std_logic;
  signal ifft_we                : std_logic;
  signal ifft_done              : std_logic;
  signal ifft_data_valid        : std_logic;

  -- Delay I Q for FFT
  signal delay_fft_data_valid             : std_logic;
  signal delay_fft_en                     : std_logic;
  signal delay_fft_i_in, delay_fft_q_in   : std_logic_vector(N_FFT - 1 downto 0);
  signal delay_fft_i_out, delay_fft_q_out : std_logic_vector(N_FFT - 1 downto 0);
  
  -- FFT
  constant N_FFT_OUT          : integer := 30;    -- word bits for IFFT output
  signal fft_i_in, fft_q_in   : std_logic_vector(N_FFT - 1 downto 0);
  signal fft_i_out, fft_q_out : std_logic_vector(N_FFT_OUT - 1 downto 0);
  signal fft_start            : std_logic;
  signal fft_we               : std_logic;
  signal fft_done             : std_logic;
  signal fft_data_valid       : std_logic;

  -- Cordic
  signal cordic_q_in, cordic_i_in : std_logic_vector(15 downto 0);
  signal cordic_en                : std_logic;
  signal cordic_data_valid        : std_logic;
  signal cordic_data_out          : std_logic_vector(15 downto 0);

  -- 16 PSK Demapper
  signal demapper_en         : std_logic;
  signal demapper_data_valid : std_logic;
  signal demapper_data_in    : std_logic_vector(15 downto 0);
  signal demapper_data_out   : std_logic_vector(3 downto 0);

  -- Viterbi decoder
  signal viterbi_ce        : std_logic; -- TODO: check
  signal viterbi_ready     : std_logic;
  signal viterbi_data_out  : std_logic_vector(0 downto 0);
  signal viterbi_data_in_1 : std_logic_vector(0 downto 0);
  signal viterbi_data_in_0 : std_logic_vector(0 downto 0);

  -- Output FIFO (fifo_1_to_8)
  signal fifo_out_wr_en   : std_logic;
  signal fifo_out_empty   : std_logic;
  signal fifo_out_rd_en   : std_logic;
  signal fifo_out_data_in : std_logic_vector(0 downto 0);

begin

  clk_sig <= clk_sys;
  clk_2x_sig <= clk_2x_sys;
  rst_sig <= fx2_rst;

  -- Input FIFO instantiation
  fifo_in: fifo_8_to_1
    port map(
      rst    => fx2_rst,
      wr_clk => fx2_clk,
      rd_clk => clk_2x_sig,
      din    => h2fData_out,
      wr_en  => h2fValid_out,
      rd_en  => fifo_in_rd_en,
      dout   => fifo_in_data_out,
      full   => open,
      empty  => fifo_in_empty,
      valid  => fifo_in_data_valid
    );

  -- Read will be enabled when input FIFO is not empty
  fifo_in_rd_en <= not fifo_in_empty;

  -- Input FIFO => Convolutional encoder
  encoder_data_in <= fifo_in_data_out(0);
  encoder_new_data_in <= fifo_in_data_valid;

  -- Convolutional encoder instantiation
  conv_enc: encoder
    port map(
      data_in    => encoder_data_in,
      data_out_v => encoder_data_out,
      rdy        => encoder_data_valid,
      nd         => encoder_new_data_in,
      ce         => '1',
      sclr       => rst_sig,
      clk        => clk_2x_sig
    );

  -- Convolutional encoder => FIFO 2 bits to 4 bits
  fifo_2to4_data_in <= encoder_data_out;
  fifo_2to4_wr_en <= encoder_data_valid;

  -- FIFO 2 bits to 4 bits instantiation
  fifo_2to4_0: fifo_2_to_4
    port map(
      rst    => rst_sig,
      wr_clk => clk_2x_sig,
      rd_clk => clk_sig,
      din    => fifo_2to4_data_in,
      wr_en  => fifo_2to4_wr_en,
      rd_en  => fifo_2to4_rd_en,
      dout   => fifo_2to4_data_out,
      full   => open,
      empty  => fifo_2to4_empty,
      valid  => fifo_2to4_data_valid
    );

  -- Read will be enabled when the FIFO is not empty
  fifo_2to4_rd_en <= not fifo_2to4_empty;

  -- FIFO 2 bits to 4 bits => 16 PSK Mapper
  mapper_en <= fifo_2to4_data_valid;
  mapper_data_in <= fifo_2to4_data_out;

  -- 16 PSK Mapper
  mapper0: mapper_16_psk
    port map (
      clk        => clk_sig,
      rst        => rst_sig,
      en         => mapper_en,
      data_in    => mapper_data_in,
      data_valid => mapper_data_valid,
      q_out      => mapper_q_out,
      i_out      => mapper_i_out
    );

  -- 16 PSK Mapper => Delay I Q IFFT
  delay_ifft_en <= mapper_data_valid;
  delay_ifft_q_in <= mapper_q_out;
  delay_ifft_i_in <= mapper_i_out;

  -- Delay I Q IFFT
  delay_ifft : delay_i_q
    generic map (N => N_IFFT)
    port map (
      clk => clk_sig,
      rst => rst_sig,
      en => delay_ifft_en,
      data_i_in => delay_ifft_i_in,
      data_q_in => delay_ifft_q_in,
      data_i_out => delay_ifft_i_out,
      data_q_out => delay_ifft_q_out,
      data_valid => delay_ifft_data_valid
    );

  -- Delay I Q IFFT => IFFT
  ifft_start <= delay_ifft_data_valid;
  ifft_i_in <= delay_ifft_i_out;
  ifft_q_in <= delay_ifft_q_out;

  -- IFFT
  ifft0: ifft
    port map (
      clk => clk_sig,
      ce => '1',
      sclr => rst_sig,
      start => ifft_start,
      unload => ifft_done,
      xn_re => ifft_i_in,
      xn_im => ifft_q_in,
      fwd_inv => '0',
      fwd_inv_we => ifft_we,
      rfd => open,
      xn_index => open,
      busy => open,
      edone => open,
      done => ifft_done,
      dv => ifft_data_valid,
      xk_index => open,
      xk_re => ifft_i_out,
      xk_im => ifft_q_out
    );

  -- IFFT => Delay I Q FFT
  delay_fft_en <= ifft_data_valid;
  delay_fft_i_in <= ifft_i_out;
  delay_fft_q_in <= ifft_q_out;

  -- Delay I Q FFT
  delay_fft : delay_i_q
    generic map (N => N_FFT)
    port map (
      clk => clk_sig,
      rst => rst_sig,
      en => delay_fft_en,
      data_i_in => delay_fft_i_in,
      data_q_in => delay_fft_q_in,
      data_valid => delay_fft_data_valid,
      data_i_out => delay_fft_i_out,
      data_q_out => delay_fft_q_out
    );

  -- Delay I Q FFT => FFT
  fft_start <= delay_fft_data_valid;
  fft_i_in <= delay_fft_i_out;
  fft_q_in <= delay_fft_q_out;

  -- FFT
  fft0: fft
    port map (
      clk => clk_sig,
      ce => '1',
      sclr => rst_sig,
      start => fft_start,
      unload => fft_done,
      xn_re => fft_i_in,
      xn_im => fft_q_in,
      fwd_inv => '1',
      fwd_inv_we => fft_we,
      rfd => open,
      xn_index => open,
      busy => open,
      edone => open,
      done => fft_done,
      dv => fft_data_valid,
      xk_index => open,
      xk_re => fft_i_out,
      xk_im => fft_q_out
    );

  -- FFT => Cordic
  cordic_en <= fft_data_valid;
  cordic_i_in <= fft_i_out(N_FFT_OUT - 1 downto N_FFT_OUT - 16);
  cordic_q_in <= fft_q_out(N_FFT_OUT - 1 downto N_FFT_OUT - 16);

  -- Cordic
  cordic0: cordic
    generic map (
      N => N_tb,
      ITER => ITER
    )
    port map (
      clk        => clk_sig,
      rst        => rst_sig,
      en         => cordic_en,
      x_in       => cordic_i_in,
      y_in       => cordic_q_in,
      data_valid => cordic_data_valid,
      z_out      => cordic_data_out
    );

  -- Cordic => 16 PSK Demapper
  demapper_en <= cordic_data_valid;
  demapper_data_in <= cordic_data_out;

  -- 16 PSK Demapper
  demapper0: demapper_16_psk
    port map (
      clk        => clk_sig,
      rst        => rst_sig,
      en         => demapper_en,
      data_in    => demapper_data_in,
      data_valid => demapper_data_valid,
      data_out   => demapper_data_out
    );

  -- FIFO 2 bits to 4 bits => FIFO 4 bits to 2 bits
  fifo_4to2_wr_en <= demapper_data_valid;
  fifo_4to2_data_in <= demapper_data_out;

  -- FIFO 4 bits to 2 bits instantiation
  fifo_4to2_0: fifo_4_to_2
    port map(
      rst    => rst_sig,
      wr_clk => clk_sig,
      rd_clk => clk_2x_sig,
      din    => fifo_4to2_data_in,
      wr_en  => fifo_4to2_wr_en,
      rd_en  => fifo_4to2_rd_en,
      dout   => fifo_4to2_data_out,
      full   => open,
      empty  => fifo_4to2_empty,
      valid  => fifo_4to2_data_valid
    );
  
  -- Read will be enabled when the FIFO is not empty
  fifo_4to2_rd_en <= not fifo_4to2_empty;

  -- FIFO 4 bits to 2 bits => Viterbi decoder
  viterbi_data_in_0(0) <= fifo_4to2_data_out(0);
  viterbi_data_in_1(0) <= fifo_4to2_data_out(1);

  -- Viterbi decoder instantiation
  viterbi_dec: viterbi
    port map(
      data_in0 => viterbi_data_in_0,
      data_in1 => viterbi_data_in_1,
      data_out => viterbi_data_out(0),
      rdy      => viterbi_ready,
      ce       => viterbi_ce,
      sclr     => rst_sig,
      clk      => clk_2x_sig
    );

  -- Enable decoder only when data valid or ready
  viterbi_ce <= '1' when fifo_4to2_data_valid = '1' or viterbi_ready = '1'
                else '0';
  
  -- Viterbi decoder => Output FIFO
  fifo_out_data_in <= viterbi_data_out;
  fifo_out_wr_en <= viterbi_ready;

  -- Output FIFO instantiation
  fifo_out: fifo_1_to_8
    port map(
      rst    => rst_sig,
      wr_clk => clk_2x_sig,
      rd_clk => fx2_clk,
      din    => fifo_out_data_in,
      wr_en  => fifo_out_wr_en,
      rd_en  => fifo_out_rd_en,
      dout   => f2hData_in,
      full   => open,
      empty  => fifo_out_empty,
      valid  => f2hValid_in
    );

  -- Read will be enabled when output FIFO is not empty
  fifo_out_rd_en <= not fifo_out_empty;

  ifft_config_process : process(clk_sys)
    variable count : integer range 0 to 21 := 0;
  begin
    if rising_edge(clk_sys) then
      if fx2_rst = '1' then
        ifft_we <= '0';
        fft_we <= '0';
      else
        count := count + 1;
        if (count = 10) then
          ifft_we <= '1';
          fft_we <= '1';
        end if;
        if (count = 11) then
          ifft_we <= '0';
          fft_we <= '0';
        end if;
        if (count > 12) then
          count := 12;
        end if;
      end if;
    end if;
  end process;

end architecture;

