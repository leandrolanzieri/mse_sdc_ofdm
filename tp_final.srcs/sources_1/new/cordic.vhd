----------------------------------------------------------------------------------
-- Company: 
-- Engineer:    Leandro Lanzieri Rodriguez
-- 
-- Create Date: 06/06/2019 10:25:30 PM
-- Design Name: 
-- Module Name: cordic_iteration - cordic_iteration_arch
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;


entity cordic is
    generic (
        N    : natural := 16; -- word bits
        ITER : natural := 16-- amount of iterations
    );
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        en         : in std_logic;
        x_in       : in std_logic_vector(N-1 downto 0);
        y_in       : in std_logic_vector(N-1 downto 0);
        data_valid : out std_logic;
        z_out      : out std_logic_vector(N-1 downto 0)
    );
end cordic;

architecture cordic_arch of cordic is
    constant MAX_ITER : natural := 9;

    component cordic_iteration is
        generic (
            N     : natural := 8; -- word bits
            SHIFT : natural := 1
        );
        port ( 
            clk        : in std_logic;
            rst        : in std_logic;
            en         : in std_logic;
            x_in       : in std_logic_vector(N-1 downto 0);
            y_in       : in std_logic_vector(N-1 downto 0);
            z_in       : in std_logic_vector(N-1 downto 0);
            c_in       : in std_logic_vector(N-1 downto 0);
            data_valid : out std_logic;
            x_out      : out std_logic_vector(N-1 downto 0);
            y_out      : out std_logic_vector(N-1 downto 0);
            z_out      : out std_logic_vector(N-1 downto 0)
        );
    end component;

    component cordic_quadrant_correction is
        generic (
            N : natural := 8    -- word bits
        );
        port (
            clk        : in std_logic;
            rst        : in std_logic;
            en         : in std_logic;
            x_in       : in std_logic_vector(N-1 downto 0);
            y_in       : in std_logic_vector(N-1 downto 0);
            z_in       : in std_logic_vector(N-1 downto 0);
            data_valid : out std_logic;
            x_out      : out std_logic_vector(N-1 downto 0);
            y_out      : out std_logic_vector(N-1 downto 0);
            z_out      : out std_logic_vector(N-1 downto 0)
        );
    end component;

    type handShakeVector is array(ITER downto 0) of std_logic;
    signal en_sig, data_valid_sig : handShakeVector;

    type connectVector is array(ITER downto 0) of std_logic_vector(N-1 downto 0);
    signal wire_x, wire_y, wire_z, wire_lut : connectVector;

--------------------------------------------------------------------------------
-- atan lookup table calculations
--      a = atan(2 ^ -i)
--------------------------------------------------------------------------------
-- i        deg          rad        int           frac          out
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- 0       45.00       0.7854      0b0000    0b110010010000    3216
--------------------------------------------------------------------------------
-- 1       26.75       0.4636      0b0000    0b011101101010    1898
--------------------------------------------------------------------------------
-- 2       14.04       0.2450      0b0000    0b001111101011    1003
--------------------------------------------------------------------------------
-- 3        7.13       0.1244      0b0000    0b000111111101     509
--------------------------------------------------------------------------------
-- 4        3.58       0.0624      0b0000    0b000011111111     255
--------------------------------------------------------------------------------
-- 5        1.79       0.0312      0b0000    0b000001111111     127
--------------------------------------------------------------------------------
-- 6        0.90       0.0160      0b0000    0b000001000001      65
--------------------------------------------------------------------------------
-- 7        0.45       0.0080      0b0000    0b000000100000      32
--------------------------------------------------------------------------------
-- 8        0.22       0.0040      0b0000    0b000000010000      16
--------------------------------------------------------------------------------
-- 9        0.11       0.0020      0b0000    0b000000001000       8
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

    type intLut is array(0 to MAX_ITER) of integer range 0 to 2**N;
    signal atan_lut : intLut := (3216, 1898, 1003, 509, 255, 127, 65, 32, 16, 8);

    signal x_to_correct : std_logic_vector(N-1 downto 0);
    signal y_to_correct : std_logic_vector(N-1 downto 0);

    signal x_corrected : std_logic_vector(N-1 downto 0);
    signal y_corrected : std_logic_vector(N-1 downto 0);
    signal z_corrected : std_logic_vector(N-1 downto 0);
begin

    corrector : cordic_quadrant_correction
    generic map (
        N => N
    )
    port map (
        clk => clk,
        rst => rst,
        en => en,
        x_in => x_to_correct,
        y_in => y_to_correct,
        z_in => (N-1 downto 0 => '0'),
        data_valid => en_sig(0),
        x_out => x_corrected,
        y_out => y_corrected,
        z_out => z_corrected
    );

    x_to_correct <= x_in;
    y_to_correct <= y_in;

    cordic_it: for j in 0 to ITER-1 generate
    begin
        wire_lut(j) <= std_logic_vector(to_signed(atan_lut(j), N));

        it0:
            if j = 0 generate
            begin
                iter0: cordic_iteration
                generic map (N, 0)
                port map (
                    clk => clk,
                    rst => rst,
                    en => en_sig(0),
                    x_in => x_corrected,
                    y_in => y_corrected,
                    z_in => z_corrected,
                    c_in => wire_lut(0),
                    data_valid => data_valid_sig(0),
                    x_out => wire_x(0),
                    y_out => wire_y(0),
                    z_out => wire_z(0)
                );
            end generate;

        itj:
            if j > 0 and j < (ITER-1) generate
            begin
                iterj: cordic_iteration
                generic map (N, j)
                port map(
                    clk => clk,
                    rst => rst,
                    en => data_valid_sig(j-1),
                    x_in => wire_x(j-1),
                    y_in => wire_y(j-1),
                    z_in => wire_z(j-1),
                    c_in => wire_lut(j),
                    data_valid => data_valid_sig(j),
                    x_out => wire_x(j),
                    y_out => wire_y(j),
                    z_out => wire_z(j)
                );
            end generate;

        itLast:
            if j = (ITER-1) generate
            begin
                iterLast: cordic_iteration
                generic map (N, j)
                port map (
                    clk => clk,
                    rst => rst,
                    en => data_valid_sig(j-1),
                    x_in => wire_x(j-1),
                    y_in => wire_y(j-1),
                    z_in => wire_z(j-1),
                    c_in => wire_lut(j),
                    data_valid => data_valid,
                    x_out => open,
                    y_out => open,
                    z_out => z_out
                );
            end generate;
        end generate;
end cordic_arch;
