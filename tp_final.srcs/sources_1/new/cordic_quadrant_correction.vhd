----------------------------------------------------------------------------------
-- Company: 
-- Engineer:    Leandro Lanzieri Rodriguez
-- 
-- Create Date: 06/06/2019 10:25:30 PM
-- Design Name: 
-- Module Name: cordic_iteration - cordic_iteration_arch
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity cordic_quadrant_correction is
    generic (
        N : natural := 8    -- word bits
    );
    port (
        clk        : in std_logic;
        rst        : in std_logic;
        en         : in std_logic;
        x_in       : in std_logic_vector(N-1 downto 0);
        y_in       : in std_logic_vector(N-1 downto 0);
        z_in       : in std_logic_vector(N-1 downto 0);
        data_valid : out std_logic;
        x_out      : out std_logic_vector(N-1 downto 0);
        y_out      : out std_logic_vector(N-1 downto 0);
        z_out      : out std_logic_vector(N-1 downto 0)
    );
end cordic_quadrant_correction;

architecture cordic_quadrant_correction_arch of cordic_quadrant_correction is
    signal x_int : integer;
    signal y_int : integer;
    signal z_int : integer;
    signal quadrant : std_logic_vector(1 downto 0);
begin

    quadrant(0) <= x_in(N-1);
    quadrant(1) <= y_in(N-1);

    main_process : process(clk)
    begin
        if (rst = '1') then
            x_out <= (others => '0');
            y_out <= (others => '0');
            z_out <= (others => '0');
            data_valid <= '0';
        else
            data_valid <= en;
            if (en = '1') then
                -- 1st and 4th quadrants need no correction
                if quadrant = "00" or quadrant = "10" then
                    x_out <= x_in;
                    y_out <= y_in;
                    z_out <= z_in;
                else
                -- 3rd quadrant mapped to 1st, 2nd mapped to 4th
                    x_out <= std_logic_vector(to_signed(-to_integer(signed(x_in)), N));
                    y_out <= std_logic_vector(to_signed(-to_integer(signed(y_in)), N));
                    z_out <= std_logic_vector(to_signed(-to_integer(signed(z_in)), N));
                end if;
            else
                x_out <= (others => '0');
                y_out <= (others => '0');
                z_out <= (others => '0');
                data_valid <= '0';
            end if;
        end if;
    end process;

end cordic_quadrant_correction_arch;
