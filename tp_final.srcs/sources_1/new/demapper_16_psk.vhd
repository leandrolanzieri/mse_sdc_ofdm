library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

entity demapper_16_psk is
  port (
    clk : in std_logic;
    rst : in std_logic;
    en  : in std_logic;
    data_in : in std_logic_vector(15 downto 0);
    data_valid : out std_logic;
    data_out : out std_logic_vector(3 downto 0)
  );
end demapper_16_psk; 

architecture demapper_16_psk_arch of demapper_16_psk is
  constant N: natural := 16;
  signal internal_data_in: integer range -2 ** (N-1) to 2 ** (N-1) - 1;
begin

--------------------------------------------------------------------------------
-- Calculations: Q12 fixed point from radians
--------------------------------------------------------------------------------

-----------------------------------------------------------------------------
-- Angle [deg] -- Angle [rad] -- Int. Part --    Frac. Part    --  Output  --
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
--     0.0     --   0.00000   --  0b0000   --  0b000000000000  --      0   --
-----------------------------------------------------------------------------
--    22.5     --   0.39269   --  0b0000   --  0b011001001000  --   1608   --
-----------------------------------------------------------------------------
--    45.0     --   0.78539   --  0b0000   --  0b110010010000  --   3216   --
-----------------------------------------------------------------------------
--    67.5     --   1.17809   --  0b0001   --  0b001011011001  --   4825   --
-----------------------------------------------------------------------------
--    90.0     --   1.57079   --  0b0001   --  0b100100100001  --   6433   --
-----------------------------------------------------------------------------
--   112.5     --   1.96349   --  0b0001   --  0b111101101010  --   8042   --
-----------------------------------------------------------------------------
--   135.0     --   2.35619   --  0b0010   --  0b010110110010  --   9050   --
-----------------------------------------------------------------------------
--   157.5     --   2.74889   --  0b0010   --  0b101111111011  --  11259   --
-----------------------------------------------------------------------------
--   180.0     --   3.14159   --  0b0011   --  0b001001000011  --  12867   --
-----------------------------------------------------------------------------
--   202.5     --   3.53429   --  0b0011   --  0b100010001100  --  14476   --
-----------------------------------------------------------------------------
--   225.0     --   3.92699   --  0b0011   --  0b111011010100  --  16084   --
-----------------------------------------------------------------------------
--   247.5     --   4.31968   --  0b0100   --  0b010100011101  --  17693   --
-----------------------------------------------------------------------------
--   270.0     --   4.71238   --  0b0100   --  0b101101100101  --  19301   --
-----------------------------------------------------------------------------
--   -90.0     --  -1.57079   --  0b1110   --  0b011011011111  --  -6433   --
-----------------------------------------------------------------------------
--   -67.5     --  -1.17809   --  0b1110   --  0b110100100111  --  -4825   --
-----------------------------------------------------------------------------
--   -45.0     --  -0.78539   --  0b1111   --  0b001101110000  --  -3216   --
-----------------------------------------------------------------------------
--   -22.5     --  -0.39269   --  0b1111   --  0b100110111000  --  -1608   --
-----------------------------------------------------------------------------

  -- convert input data to an integer always
  internal_data_in <= to_integer(signed(data_in));

  main_process: process(clk)
  begin
    if rising_edge(clk) then
      if (rst = '1') then
        -- reset output and data valid
        data_valid <= '0';
        data_out <= (others => '0');
      else
        if en = '1' then
          -- Check between which angles the input is
          -- 0 < x <= 22.5
          if (internal_data_in > 0 and internal_data_in <= 1608) then
            data_out <= "0000";
            data_valid <= '1';
          -- 22.5 < x <= 45
          elsif (internal_data_in > 1608 and internal_data_in <= 3216) then
            data_out <= "0001";
            data_valid <= '1';
          -- 45 < x <= 67.5
          elsif (internal_data_in > 3216 and internal_data_in <= 4825) then
            data_out <= "0010";
            data_valid <= '1';
          -- 67.5 < x <= 90
          elsif (internal_data_in > 4825 and internal_data_in <= 6433) then
            data_out <= "0011";
            data_valid <= '1';
          -- 90 < x <= 112.5
          elsif (internal_data_in > 6433 and internal_data_in <= 8042) then
            data_out <= "0100";
            data_valid <= '1';
          -- 112.5 < x <= 135
          elsif (internal_data_in > 8042 and internal_data_in <= 9050) then
            data_out <= "0101";
            data_valid <= '1';
          -- 135 < x <= 157.5
          elsif (internal_data_in > 9050 and internal_data_in <= 11259) then
            data_out <= "0110";
            data_valid <= '1';
          -- 157.5 < x <= 180
          elsif (internal_data_in > 11259 and internal_data_in <= 12867) then
            data_out <= "0111";
            data_valid <= '1';
          -- 180 < x <= 202.5
          elsif (internal_data_in > 12867 and internal_data_in <= 14476) then
            data_out <= "1000";
            data_valid <= '1';
          -- 202.5 < x <= 225
          elsif (internal_data_in > 14476 and internal_data_in <= 16084) then
            data_out <= "1001";
            data_valid <= '1';
          -- 225 < x <= 247.5
          elsif (internal_data_in > 16084 and internal_data_in <= 17693) then
            data_out <= "1010";
            data_valid <= '1';
          -- 247.5 < x <= 270
          elsif (internal_data_in > 17693 and internal_data_in <= 19301) then
            data_out <= "1011";
            data_valid <= '1';
          -- -90 < x <= -67.5
          elsif (internal_data_in > -6433 and internal_data_in <= -4825) then
            data_out <= "1100";
            data_valid <= '1';
          -- -67.5 < x <= -45
          elsif (internal_data_in > -4825 and internal_data_in <= -3216) then
            data_out <= "1101";
            data_valid <= '1';
          -- -45 < x <= -22.5
          elsif (internal_data_in > -3216 and internal_data_in <= -1608) then
            data_out <= "1110";
            data_valid <= '1';
          -- -22.5 < x <= -0
          elsif (internal_data_in > -1608 and internal_data_in <= -0) then
            data_out <= "1111";
            data_valid <= '1';
          else
            data_out <= "0000";
            data_valid <= '0';
          end if;
        else
          -- no valid data when not enabled
          data_valid <= '0';
        end if;
      end if;
    end if;
  end process;

end architecture;